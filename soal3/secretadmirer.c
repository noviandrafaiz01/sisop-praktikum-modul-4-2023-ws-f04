#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <ctype.h>

static
const char * dirpath = "/home/fathan/inifolderetc/sisop";
static
const char * passwd = "1234";

const char base64Chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeFile(const char * filePath) {
   FILE * file = fopen(filePath, "rb");
   if (!file) {
      fprintf(stderr, "Gagal membuka file: %s\n", filePath);
      return;
   }

   // hitung ukuran file
   fseek(file, 0, SEEK_END);
   long fileSize = ftell(file);
   fseek(file, 0, SEEK_SET);

   // Alokasikan memory untuk isi file
   unsigned char * fileContent = malloc(fileSize);
   if (!fileContent) {
      fprintf(stderr, "Gagal mengalokasikan memori.\n");
      fclose(file);
      return;
   }

   // baca isi file
   if (fread(fileContent, 1, fileSize, file) != fileSize) {
      fprintf(stderr, "Gagal membaca file: %s\n", filePath);
      fclose(file);
      free(fileContent);
      return;
   }

   // hitung ukuran output base64
   long outputSize = 4 * ((fileSize + 2) / 3);

   // alokasikan memori untuk output base64
   char * base64Output = malloc(outputSize + 1);
   if (!base64Output) {
      fprintf(stderr, "Gagal Mengalokasikan memori.\n");
      fclose(file);
      free(fileContent);
      return;
   }

   // Encode file ke base64
   int i, j;
   for (i = 0, j = 0; i < fileSize; i += 3, j += 4) {
      unsigned char byte1 = fileContent[i];
      unsigned char byte2 = (i + 1 < fileSize) ? fileContent[i + 1] : 0;
      unsigned char byte3 = (i + 2 < fileSize) ? fileContent[i + 2] : 0;

      unsigned char index1 = byte1 >> 2;
      unsigned char index2 = ((byte1 & 0x03) << 4) | (byte2 >> 4);
      unsigned char index3 = ((byte2 & 0x0F) << 2) | (byte3 >> 6);
      unsigned char index4 = byte3 & 0x3F;

      base64Output[j] = base64Chars[index1];
      base64Output[j + 1] = base64Chars[index2];
      base64Output[j + 2] = (i + 1 < fileSize) ? base64Chars[index3] : '=';
      base64Output[j + 3] = (i + 2 < fileSize) ? base64Chars[index4] : '=';
   }
   base64Output[j] = '\0';

   // tutup file
   fclose(file);

   // write encoded base64 ke temporary file
   FILE * tempFile = fopen("temp.txt", "wb");
   if (!tempFile) {
      fprintf(stderr, "Gagal membuat temporary file.\n");
      free(fileContent);
      free(base64Output);
      return;
   }

   if (fwrite(base64Output, 1, strlen(base64Output), tempFile) != strlen(base64Output)) {
      fprintf(stderr, "Gagal melakukan write ke temporary file\n");
   }

   // tutup temporary file
   fclose(tempFile);

   // hapus file original
   if (remove(filePath) != 0) {
      fprintf(stderr, "Gagal menghapus original file: %s\n", filePath);
      free(fileContent);
      free(base64Output);
      return;
   }

   // ganti nama temporary file ke nama file original
   if (rename("temp.txt", filePath) != 0) {
      fprintf(stderr, "Gagal menngati nama temporary file.\n");
   } else {
      // printf("File berhasil di encoded\n");
   }

   // bersihkan resource
   free(fileContent);
   free(base64Output);
}

void encode(const char * path, int flag) {
   DIR * dir;
   struct dirent * entry;
   struct stat fileStat;

   // buka direktory
   dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Direktori gagal untuk dibuka: %s\n", path);
      return;
   }

   // baca entri direktori
   while ((entry = readdir(dir)) != NULL) {

      // abaikan direktori sekrang dan direktori parent
      if (strcmp(entry -> d_name, ".") == 0 || strcmp(entry -> d_name, "..") == 0) {
         continue;
      }

      // buat full path untuk entri
      char fullpath[1024];
      snprintf(fullpath, sizeof(fullpath), "%s/%s", path, entry -> d_name);

      // dapatkan info entri
      if (stat(fullpath, & fileStat) < 0) {
         fprintf(stderr, "Failed to get file stat: %s\n", fullpath);
         continue;
      }

      if (S_ISDIR(fileStat.st_mode)) {
         // Entry adalah direktori
         // printf("Directory: %s\n", fullpath);

         // int flag = 0;

         char firstLetter = entry -> d_name[0];
         if (flag == 1 || firstLetter == 'l' || firstLetter == 'u' || firstLetter == 't' || firstLetter == 'h' || firstLetter == 'L' || firstLetter == 'U' || firstLetter == 'T' || firstLetter == 'H') {
            encode(fullpath, 1);
         } else {
            encode(fullpath, 0);
         }

         //encode direktorii secara rekursif

      } else if (S_ISREG(fileStat.st_mode)) {
         // entry adalah file biasa

         if (flag == 1) {
            // encode
            encodeFile(fullpath);
            // printf("File: %d %s\n", flag, fullpath);

         }
         //printf("File: %d %s\n", flag, fullpath);
      }
   }

   // tutup direktori
   closedir(dir);
}

void formatAscii(char * name) {
   if (strlen(name) <= 4) {
      char newName[256];
      int div;
      size_t index = 0;
      for (int i = 0; i < strlen(name); i++) {
         div = 128; // 8th bit
         while (div) {
            newName[index] = ((name[i] & div) == div) ? '1' : '0';
            index++;
            if (div == 1 && i < strlen(name) - 1) {
               newName[index] = ' ';
               index++;
            }
            div /= 2;
         }
      }
      newName[index] = '\0';
      strcpy(name, newName);
   }

}

void format(const char * path) {
   DIR * dir = opendir(path);
   if (!dir) {
      fprintf(stderr, "Gagal membuka direktori: %s\n", path);
      return;
   }

   struct dirent * entry;
   while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) {
         // buat full path
         char oldFilePath[1024];
         snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", path, entry -> d_name);

         // buat nama file dengan lowercase
         char newFileName[1024];
         strcpy(newFileName, entry -> d_name);
         int len = strlen(newFileName);
         for (int i = 0; i < len; i++) {
            newFileName[i] = tolower(newFileName[i]);
         }

         // buat full path file baru
         char newFilePath[1030];
         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);

         len = strlen(newFileName);
         if (len <= 4) {
            char newName[256] = "";
            int div;
            size_t index = 0;

            for (int i = 0; i < len; i++) {
               div = 128; // 8th bit
               while (div) {
                  newName[index] = ((newFileName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }
            }

            strcpy(newFileName, newName);
         }

         snprintf(newFilePath, sizeof(newFilePath), "%s/%s", path, newFileName);

         // Ganti nama file
         if (rename(oldFilePath, newFilePath) != 0) {
            fprintf(stderr, "Gagal mengganti nama file: %s\n", oldFilePath);
            continue;
         } else {
            // printf("sukses mengganti nama file: %s\n", newFilePath);
         }

      } else if (entry -> d_type == DT_DIR && strcmp(entry -> d_name, ".") != 0 && strcmp(entry -> d_name, "..") != 0) {
         // buat full path direktoru
         char oldDirPath[1024];
         snprintf(oldDirPath, sizeof(oldDirPath), "%s/%s", path, entry -> d_name);

         // buat nama direktori baru dengan uppercase semua
         char newDirName[1024];
         strcpy(newDirName, entry -> d_name);
         int len = strlen(newDirName);
         for (int i = 0; i < len; i++) {
            newDirName[i] = toupper(newDirName[i]);
         }

         // buat full path direktori baru
         char newDirPath[1500];
         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);

         len = strlen(newDirName);
         if (len <= 4) {

            char newName[256] = "";
            int div;
            size_t index = 0;

            for (int i = 0; i < len; i++) {

               div = 128; // 8th bit
               while (div != 0) {
                  newName[index] = ((newDirName[i] & div) == div) ? '1' : '0';
                  index++;
                  if (div == 1 && i < len - 1) {
                     newName[index] = ' ';
                     index++;
                  }
                  div /= 2;
               }

            }

            strcpy(newDirName, newName);
         }

         snprintf(newDirPath, sizeof(newDirPath), "%s/%s", path, newDirName);

         // Ganti nama direktori
         if (rename(oldDirPath, newDirPath) != 0) {
            fprintf(stderr, "Gagal mengganti nama direktori: %s\n", oldDirPath);
            continue;
         } else {
            // printf("sukses mengganti nama direktori: %s\n", newDirPath);
         }

         // pannggil function secara rekursif

         format(newDirPath);
      }

   }

   // Close the directory
   closedir(dir);
}

static void * xmp_init(struct fuse_conn_info * conn) {
   (void) conn;
   char fpath[1000];

   const char * path = "/";
   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   encode(fpath, 0);

   format(fpath);

   return NULL;
}

static int xmp_getattr(const char * path, struct stat * stbuf) {
   int res;
   char fpath[1000];

   sprintf(fpath, "%s%s", dirpath, path);

   res = lstat(fpath, stbuf);

   if (res == -1) return -errno;

   return 0;
}

static int xmp_readdir(const char * path, void * buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info * fi) {

   // printf("call readdir(): %s\n", path);

   char fpath[1000];

   if (strcmp(path, "/") == 0) {
      path = dirpath;
      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   int res = 0;

   DIR * dp;
   struct dirent * de;
   (void) offset;
   (void) fi;

   dp = opendir(fpath);

   if (dp == NULL) return -errno;

   while ((de = readdir(dp)) != NULL) {
      struct stat st;

      memset( & st, 0, sizeof(st));

      st.st_ino = de -> d_ino;
      st.st_mode = de -> d_type << 12;
      res = (filler(buf, de -> d_name, & st, 0));

      if (res != 0) break;
   }

   closedir(dp);

   return 0;
}

static int xmp_read(const char * path, char * buf, size_t size, off_t offset, struct fuse_file_info * fi) {
   char fpath[1000];
   if (strcmp(path, "/") == 0) {
      path = dirpath;

      sprintf(fpath, "%s", path);
   } else sprintf(fpath, "%s%s", dirpath, path);

   int res = 0;
   int fd = 0;

   (void) fi;

   fd = open(fpath, O_RDONLY);

   if (fd == -1) return -errno;

   res = pread(fd, buf, size, offset);

   if (res == -1) res = -errno;

   close(fd);

   return res;
}

static struct fuse_operations xmp_oper = {
   .init = xmp_init,
   .getattr = xmp_getattr,
   .readdir = xmp_readdir,
   .read = xmp_read,

};

int main(int argc, char * argv[]) {
   umask(0);

   return fuse_main(argc, argv, & xmp_oper, NULL);

}